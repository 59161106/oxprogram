import java.util.Scanner;

public class ox {

    public static void printWelcome(){
        System.out.println("Welcome to OX Game");
    }

    static String[][] board = {{"  ", "1 ","2 ","3 "},{"1 ", "- ", "- ", "- "},{"2 ", "- ", "- ", "- "},{"3 ", "- ", "- ", "- "}};
    public static void printBoard(){   	
    	for(int i=0; i<4; i++) {
    		for(int j=0; j<4; j++) {
    			System.out.print(board[i][j]);
    		}System.out.println();
    	}
    	
    }
    
    static String player = "X ";
    
    public static void choosePlayer() {
    	Scanner kb = new Scanner(System.in);
    	String choose;
    	System.out.print("You want to play X or O : ");
    	choose = kb.next();
    	if(choose.equals("X") || choose.equals("x")) {
    		player = "O ";
    	}else if(choose.equals("O") || choose.equals("o")){
    		player = "X "; 
    	}else {
    		System.out.println();
    		System.out.println("You choose wrong please choose again");
    		System.out.println();
    		choosePlayer();
    	}
    }
    
    public static void printTurn(){
    	if(player == "X ") {
    		player = "O ";
    		System.out.println(player + "Turn");
    	}else {
    		player = "X ";
    		System.out.println(player + "Turn");
    	}
    		
    }

    public static void Input(){
    	Scanner kb = new Scanner(System.in);
    	int row,col;
   
    	System.out.print("Please Input Row Col : ");
    	row = kb.nextInt();
    	col = kb.nextInt();
    	
    	if(board[row][col].equals("- ") && (row <= 3 && col <= 3)) {
    		board[row][col] = player;
    		System.out.println();
    	}else {
    		countD--;
    		System.out.println();
    		System.out.println("Please check your position and play agin.");
    		System.out.println();
    		if(player == "X ") {
    			player = "O ";
    		}else {
    			player = "X ";
    		}

    	}
    	
    }
    
    static boolean checkWin = false;
    static int count = 0;
    public static void checksWin(){
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(board[j][i] == player) {
    				count++;
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    	for(int i=0; i<4; i++) {
    		count = 0;
    		for(int j=0; j<4; j++) {
    			if(board[i][j] == player) {
    				count++;
    			}
    		}
    		if(count == 3) {
    			checkWin = true;
    		}
    	}
    	
    	if((board[1][1] == "X " && board[2][2] == "X " && board[3][3] == "X ") || 
    		(board[1][3] == "X " && board[2][2] == "X " && board[3][1] == "X ") ||
    		(board[1][1] == "O " && board[2][2] == "O " && board[3][3] == "O ") || 
    		(board[1][3] == "O " && board[2][2] == "O " && board[3][1] == "O ")) {
    		checkWin = true;
    	}
    	

    }
    
    public static void printWin(){
    	if(checkWin == true) {
    		System.out.println(player + "Win");
    	}else {
    		System.out.println("DRAW");
    	}
    	
    }

    public static void printBye(){
    	System.out.println("BYE BYE!!!");
    }
    
    static int countD = 0;
    public static void main(String[] args){
        printWelcome();
        choosePlayer();
        while(checkWin == false) {
        	printBoard();
        	printTurn();
        	Input();
        	countD++;
        	checksWin();
        	if(checkWin == true) {
        		break;
        	}else if(checkWin == false && countD == 9 ){
        		break;
        	}
        }
        printBoard();
        printWin();
        printBye();
        	
    
    }
}